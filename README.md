# Get Started #

## Instruction - how to build server on localhost ##

### Clone project from repository ###
```sh
git clone https://Adsel@bitbucket.org/Adsel/po3-flexgoalsserver.git
```

### Running server ###
Run default-saved configuration in Intellij idea

# Api end-points #

| METHOD | URL | PARAM | RETURNS | DESCRIPTION | 
| ------ | ------ | ------ | ------ | ------ | 
| ![#f0e609](https://via.placeholder.com/15/f0e609/000000?text=+) `POST` | 127.0.0.1:8080/api/users/login-user | AuthData | User | log in website |
| ![#f0e609](https://via.placeholder.com/15/f0e609/000000?text=+) `POST` | 127.0.0.1:8080/api/users/register-user | User | User registered_user | register account |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/users/priv-data/{id_user} | Integer id_user | UserData | get user's personal information |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/users/priv-data/update-name | UserData | UserData updated_user_data | update user's name in personal information |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/users/priv-data/update-sex | UserData | UserData updated_user_data | update user's sex in personal information |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/users/priv-data/update-date | UserData | UserData updated_user_data | update user's birth date in personal information |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/users/points/{id_user} | Integer id_user | Integer points | get actual user's points |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/predefined-final | - | PredefinedFinalGoal[] | get all predefined final goals | 
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/final/{id_user} | Integer id_user | FinalGoalFlag[] | get all active user's final goals |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/final-id/{id_goal} | Integer id_goal | FinalGoal | get final goal with specific id |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/final-shared/{id_user} | Integer id_user | FinalGoal[] | get shared final goals by another users |
| ![#f0e609](https://via.placeholder.com/15/f0e609/000000?text=+) `POST` | 127.0.0.1:8080/api/goals/final-add | FinalGoalData | FinalGoal added_goal | add new custom final goal |
| ![#ff5733](https://via.placeholder.com/15/ff5733/000000?text=+) `DELETE` | 127.0.0.1:8080/api/goals/final-delete/{id_goal} | Integer id_goal | - | delete final goal with specific id |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/goals/update-prog-fgoal | Integer id_goal | Integer code_result | score a final goal with specific id |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/goals/update-fgoal | FinalGoal | Integer code_result | update final goal's data |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api//goals/predefined-quantitative | - | PredefinedQuantitative[] | get all predefined quantitative goals |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/quantitative/{id_user} | Integer id_user | QuantitativeGoal | get all active user's quantitative goals  |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/quantitative-shared/{id_user} | Integer id_goal | QuantitativeGoal[] | get all shared quantitative goals by another users | |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/goals/quantitative-id/{id_goal} | Integer id_goal | QuantitativeGoal | get quantitative goal with specific id |
| ![#ff5733](https://via.placeholder.com/15/ff5733/000000?text=+) `DELETE` | 127.0.0.1:8080/api/goals/quantitative-delete/{id_goal} | Integer id_goal | - | delete quantitative goal with specific id |
| ![#f0e609](https://via.placeholder.com/15/f0e609/000000?text=+) `POST` | 127.0.0.1:8080/api/goals/quantitative-add | QuantitativeGoalData | QuantitativeGoal | add new custom quantitative goal |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/goals/update-qgoal | QuantitativeGoalProgress | Integer code_result | score a quantitative goal |
| ![#0093ff](https://via.placeholder.com/15/0093ff/000000?text=+) `PUT` | 127.0.0.1:8080/api/goals/update-qgoaldata | QuantitativeGoal | Integer code_result | update quantitative goal's data |
| ![#1bff00](https://via.placeholder.com/15/1bff00/000000?text=+) `GET` | 127.0.0.1:8080/api/paths/predefined | - | Path[] | get all predefined paths |
| ![#f0e609](https://via.placeholder.com/15/f0e609/000000?text=+) `POST` | 127.0.0.1:8080/api/paths/set | PathData | Integer code_status | use path to replace current goals with predefined in path |

# Instruction - how to build server on own hosting #

### Build executable jar file ###
https://www.youtube.com/watch?v=vkeB19pqA1g

### Screen usage to create remote terminal ###
https://www.interserver.net/tips/kb/using-screen-to-attach-and-detach-console-sessions/

### Starting file in bash ###
```sh
#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

PTH=""
START="java -Xms312M -Xmx512M -jar flex_goals_server-1.0.jar -o true"
NAME="flexGoalsServer"
if  screen -list | grep -q "${NAME}" ; then
    screen -d ${NAME}
    screen -r ${NAME}
else
    screen -L -S ${NAME} -d -m ${START}
    echo -e "${GREEN}Serwer${NC} ${RED}${NAME}${NC} zostal uruchomiony\n"
fi
```