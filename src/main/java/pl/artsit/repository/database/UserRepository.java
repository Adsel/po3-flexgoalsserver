package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.artsit.model.AuthData;
import pl.artsit.model.database.User;
import pl.artsit.model.database.UserData;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    @Query(
            value = "SELECT * FROM project.user u WHERE u.login = ?1",
            nativeQuery = true
    ) User login(String login);

    @Query(
            value = "SELECT u.email FROM project.user u WHERE u.email = ?1",
            nativeQuery = true
    ) String checkEmail(String email);

    @Transactional
    @Modifying
    @Query(
            value = "INSERT INTO project.user VALUES (:login, :password, :points, :email)",
            nativeQuery = true
    ) void insertUser(
            @Param("login") String login, @Param("password") String password,
            @Param("email") String email, @Param("points") Integer points
    );

    @Transactional
    @Modifying
    @Query(
            value = "INSERT INTO project.user_data VALUES (:id, :name, :dateOfBirth, :sex)",
            nativeQuery = true
    ) void insertUserData(
            @Param("id") Integer id, @Param("name") String name,
            @Param("dateOfBirth") Date date, @Param("sex") Boolean sex
    );

    @Query(
            value = "SELECT points FROM project.user WHERE id = :id",
            nativeQuery = true
    ) Integer getPoints(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.user SET points = :points WHERE id = :id",
            nativeQuery = true
    ) Integer updatePoints(@Param("id") Integer id, @Param("points") Integer points);
}
