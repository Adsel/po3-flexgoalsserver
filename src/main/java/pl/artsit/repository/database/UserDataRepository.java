package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.artsit.model.database.UserData;
import java.util.Date;

@Repository
public interface UserDataRepository extends CrudRepository<UserData, Integer> {
    @Query(
            value = "SELECT * FROM project.user_data WHERE id = :id",
            nativeQuery = true
    ) UserData getUserPrivateData(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.user_data SET name = :name WHERE id = :id",
            nativeQuery = true
    ) Integer updateUserName(@Param("id") Integer id, @Param("name") String name);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.user_data SET sex = :sex WHERE id = :id",
            nativeQuery = true
    ) Integer updateUserSex(@Param("id") Integer id, @Param("sex") Boolean sex);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.user_data SET date_of_birth = :date WHERE id = :id",
            nativeQuery = true
    ) Integer updateUserDate(@Param("id") Integer id, @Param("date") Date date);
}
