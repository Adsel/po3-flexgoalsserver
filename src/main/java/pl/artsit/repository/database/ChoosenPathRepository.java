package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.artsit.model.database.ChoosenPath;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChoosenPathRepository extends CrudRepository<ChoosenPath, Integer> {
    @Query("select u from ChoosenPath u where u.id = ?1")
    Optional<ChoosenPath> findById(Integer id);

    @Query("select u from ChoosenPath u")
    List<ChoosenPath> getChoosenPath();
}
