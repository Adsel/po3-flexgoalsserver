package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.artsit.model.database.PredefinedFinalGoal;
import pl.artsit.model.database.PredefinedQuantitativeGoal;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface PredefinedQuantitativeGoalRepository extends CrudRepository<PredefinedQuantitativeGoal, Integer> {
    @Query(
            value = "SELECT * FROM project.predefined_quantitative_goal",
            nativeQuery = true
    )
    ArrayList<PredefinedQuantitativeGoal> getPredefinedQuantitativeGoals();
}
