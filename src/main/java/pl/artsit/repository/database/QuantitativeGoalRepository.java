package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.artsit.model.database.FinalGoal;
import pl.artsit.model.database.QuantitativeGoal;
import java.util.ArrayList;
import java.util.Date;

@Repository
public interface QuantitativeGoalRepository extends CrudRepository<QuantitativeGoal, Integer> {
    @Query(
            value = "SELECT * FROM project.quantitative_goal q WHERE q.id_user = :id",
            nativeQuery = true
    )
    ArrayList<QuantitativeGoal> getQuantitativeGoals(@Param("id") Integer id);

    @Query(
            value = "SELECT * FROM project.quantitative_goal f WHERE f.is_shared = True AND f.id_user <> :id_user",
            nativeQuery = true
    ) ArrayList<QuantitativeGoal> getSharedQuantitativeGoals(Integer id_user);

    @Transactional
    @Modifying
    @Query("DELETE FROM QuantitativeGoal f where f.id = :id")
    int deleteQuantitativeGoal(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("DELETE FROM QuantitativeGoal f where f.id_user = :id")
    int deleteQuantitativeGoals(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query(
            value = "INSERT INTO project.quantitative_goal VALUES (:name, :description, :points, :is_shared, :id_user, :days, :goal, :progress, :target, :step, :date)",
            nativeQuery = true
    ) void addQuantitativeGoal(
            @Param("name") String name, @Param("description") String description, @Param("goal") String goal,
            @Param("days") Integer days, @Param("points") Integer points, @Param("is_shared") Boolean is_shared,
            @Param("progress") String progress, @Param("id_user") Integer id_user, @Param("date") Date date,
            @Param("step") Integer step, @Param("target") Integer target
    );

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.quantitative_goal SET progress = :progress WHERE id = :id",
            nativeQuery = true
    ) Integer updateQProgress(@Param("id") Integer id, @Param("progress") String progress);

    @Query(
            value = "SELECT * FROM project.quantitative_goal f WHERE f.id = :id",
            nativeQuery = true
    ) QuantitativeGoal getQuantitativeGoalWithId(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.quantitative_goal SET name = :name, description = :description, is_shared = :is_shared, goal = :goal, days = :days, step = :step WHERE id = :id",
            nativeQuery = true
    ) Integer updateQGoalData(
            @Param("id") Integer id, @Param("name") String name, @Param("description") String description,
            @Param("is_shared") Boolean is_shared, @Param("goal") String goal, @Param("days") Integer days,
            @Param("step") Integer step
    );
}
