package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.artsit.model.database.FinalGoal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public interface FinalGoalRepository extends CrudRepository<FinalGoal, Integer> {
    @Query(
            value = "SELECT * FROM project.final_goal f WHERE f.id_user = :id_user",
            nativeQuery = true
    ) ArrayList<FinalGoal> getFinalGoals(
            @Param("id_user") Integer id_user
    );

    @Transactional
    @Modifying
    @Query(
            value = "INSERT INTO project.final_goal VALUES (:name, :description, :goal, :days, :points, :is_shared, :progress, :id_user, :date)",
            nativeQuery = true
    ) void addFinalGoal(
        @Param("name") String name, @Param("description") String description, @Param("goal") String goal,
        @Param("days") Integer days, @Param("points") Integer points, @Param("is_shared") Boolean is_shared,
        @Param("progress") String progress, @Param("id_user") Integer id_user, @Param("date") Date date
    );

    @Query(
            value = "SELECT * FROM project.final_goal f WHERE f.is_shared = True AND f.id_user <> :id_user",
            nativeQuery = true
    ) ArrayList<FinalGoal> getSharedFinalGoals(Integer id_user);

    @Transactional
    @Modifying
    @Query("DELETE FROM FinalGoal f where f.id = :id")
    int deleteFinalGoal(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("DELETE FROM FinalGoal f WHERE f.id_user = :id")
    int deleteFinalGoals(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.final_goal SET progress = :progress WHERE id = :id",
            nativeQuery = true
    ) Integer updateProgress(@Param("id") Integer id, @Param("progress") String progress);

    @Query(
            value = "SELECT * FROM project.final_goal f WHERE f.id = :id",
            nativeQuery = true
    ) FinalGoal getFinalGoalWithId(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE project.final_goal SET name = :name, description = :description, is_shared = :is_shared, goal = :goal, days = :days WHERE id = :id",
            nativeQuery = true
    ) Integer updateFGoalData(
            @Param("id") Integer id, @Param("name") String name, @Param("description") String description,
            @Param("is_shared") Boolean is_shared, @Param("goal") String goal, @Param("days") Integer days
    );
}
