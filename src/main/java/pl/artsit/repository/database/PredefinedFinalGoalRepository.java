package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.artsit.model.database.PredefinedFinalGoal;
import java.util.ArrayList;

@Repository
public interface PredefinedFinalGoalRepository extends CrudRepository<PredefinedFinalGoal, Integer> {
    @Query(
            value = "SELECT * FROM project.predefined_final_goal",
            nativeQuery = true
    )
    ArrayList<PredefinedFinalGoal> getPredefinedFinalGoals();
}
