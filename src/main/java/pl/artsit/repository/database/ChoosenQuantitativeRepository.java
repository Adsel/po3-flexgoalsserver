package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.artsit.model.database.ChoosenQuantitative;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChoosenQuantitativeRepository extends CrudRepository<ChoosenQuantitative, Integer> {
    @Query("select u from ChoosenQuantitative u where u.id = ?1")
    Optional<ChoosenQuantitative> findById(Integer id);

    @Query("select u from ChoosenQuantitative u")
    List<ChoosenQuantitative> getChoosenQuantitative();
}
