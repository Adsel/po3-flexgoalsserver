package pl.artsit.repository.database;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.artsit.model.database.Path;
import java.util.ArrayList;

@Repository
public interface PathRepository extends CrudRepository<Path, Integer> {
    @Query(
            value = "SELECT * FROM project.path",
            nativeQuery = true
    )
    ArrayList<Path> getPaths();

    @Query(
            value = "SELECT * FROM project.path WHERE path = :id",
            nativeQuery = true
    )
    ArrayList<Path> getPath(@Param("id") Integer id);
}
