package pl.artsit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.artsit.model.database.QuantitativeGoal;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class QuantitativeGoalFlag {
    private Integer id;
    private String name;
    private String description;
    private Integer points;
    private Boolean is_shared;
    private Integer id_user;
    private Integer days;
    private String goal;
    private String progress;
    private Integer target;
    private Integer step;
    private Date date;
    private Integer flag;

    public QuantitativeGoalFlag(QuantitativeGoal f, Integer flag){
        this.id = f.getId();
        this.name = f.getName();
        this.description = f.getDescription();
        this.goal = f.getGoal();
        this.days = f.getDays();
        this.points = f.getPoints();
        this.is_shared = f.getIs_shared();
        this.progress = f.getProgress();
        this.id_user = f.getId_user();
        this.target = f.getTarget();
        this.step = f.getStep();
        this.date = f.getDate();
        this.flag = flag;
    }
}
