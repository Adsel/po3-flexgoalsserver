package pl.artsit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.artsit.model.database.FinalGoal;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class FinalGoalFlag extends FinalGoal {
    private Integer id;
    private String name;
    private String description;
    private String goal;
    private Integer days;
    private Integer points;
    private Boolean is_shared;
    private String progress;
    private Integer id_user;
    private Date date;
    private Integer flag;

    public FinalGoalFlag(FinalGoal f, Integer flag){
        this.id = f.getId();
        this.name = f.getName();
        this.description = f.getDescription();
        this.goal = f.getGoal();
        this.days = f.getDays();
        this.points = f.getPoints();
        this.is_shared = f.getIs_shared();
        this.progress = f.getProgress();
        this.id_user = f.getId_user();
        this.date = f.getDate();
        this.flag = flag;
    }
}
