package pl.artsit.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity

@Table(name = "ChoosenPath", schema = "project")
public class ChoosenPath {
    @Id
    @Column(name="id", nullable = false, unique=true)
    private Integer id;
    @Column(name="id_goal", nullable = false, unique=true)
    private Integer id_goal;
    @Column(name="id_user", nullable = false, unique=true)
    private Integer id_user;
    @Column(name="date")
    private Date date;
}
