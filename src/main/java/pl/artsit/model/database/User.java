package pl.artsit.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "User", schema = "project")
public class User {
    @Id
    @Column(name="id", nullable = false, unique=true)
    private Integer id;

    @Column(name="password", nullable = false)
    private String password;

    @Column(name="login", nullable = false, unique=true)
    private String login;

    @Column(name="points")
    private Integer points;

    @Column(name="email", nullable = false, unique=true)
    private String email;
}
