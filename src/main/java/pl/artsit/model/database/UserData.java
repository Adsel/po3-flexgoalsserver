package pl.artsit.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "UserData", schema = "project")
public class UserData {
    @Id
    @Column(name="id", nullable = false, unique=true)
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="date_of_birth")
    private Date date_of_birth;

    @Column(name="sex")
    private Boolean sex;
}
