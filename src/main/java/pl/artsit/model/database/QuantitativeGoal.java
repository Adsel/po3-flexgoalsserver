package pl.artsit.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity

@Table(name = "quantitative_goal", schema = "project")
public class QuantitativeGoal {
    @Id
    @Column(name="id", nullable = false, unique=true)
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="points")
    private Integer points;

    @Column(name="is_shared")
    private Boolean is_shared;

    @Column(name="id_user", nullable = false)
    private Integer id_user;

    @Column(name="days")
    private Integer days;

    @Column(name="goal")
    private String goal;

    @Column(name="progress")
    private String progress;

    @Column(name="target")
    private Integer target;

    @Column(name="step")
    private Integer step;

    @Column(name="date")
    private Date date;
}
