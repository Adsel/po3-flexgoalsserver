package pl.artsit.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class QuantitativeGoalUpdateData {
    private Integer id;
    private String name;
    private String description;
    private Integer points;
    private Boolean is_shared;
    private Integer id_user;
    private Integer days;
    private String goal;
    private String progress;
    private Integer target;
    private Integer step;
}
