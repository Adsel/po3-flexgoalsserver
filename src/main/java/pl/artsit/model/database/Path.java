package pl.artsit.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity

@Table(name = "Path", schema = "project")
public class Path {
    @Id
    @Column(name="id", nullable = false, unique=true)
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="goal")
    private String goal;

    @Column(name="days")
    private Integer days;

    @Column(name="step")
    private Integer step;

    @Column(name="path")
    private Integer path;

    @Column(name="type")
    private Boolean type;
}
