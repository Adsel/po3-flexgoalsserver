package pl.artsit.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ServerDate {
    private Integer year;
    private Integer month;
    private Integer day;

    public Integer toDays(){
        Integer days = 0;
        Integer[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for(int y = 1800; y < year; y++) {
            if (isLapYear()) {
                days += 366;
            }
            else{
                days += 365;
            }
        }
        for(int m = 0; m < month; m++) {
            if (m == 1 && isLapYear()) {
                days += 29;
            } else {
                days += daysInMonth[m];
            }
        }
        days += day;
        return days;
    }

    private Boolean isLapYear(){
        if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
            return true;
        }
        return false;
    }

    public Integer compareDates(ServerDate s2){
        //System.out.println(year+"-"+month+"-"+day+ "  -  " + s2.getYear() + "-"+s2.getMonth()+"-"+s2.getDay()+"  =  "+(this.toDays() - s2.toDays()));
        return this.toDays() - s2.toDays();
    }
}
