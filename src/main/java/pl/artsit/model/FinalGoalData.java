package pl.artsit.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FinalGoalData {
    private Integer id_user;
    private Boolean is_shared;
    private String name;
    private String description;
    private String goal;
    private Integer days;
}
