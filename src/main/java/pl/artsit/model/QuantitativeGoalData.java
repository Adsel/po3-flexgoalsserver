package pl.artsit.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class QuantitativeGoalData {
    private String name;
    private String description;
    private Boolean is_shared;
    private Integer id_user;
    private Integer days;
    private String goal;
    private Integer step;
}
