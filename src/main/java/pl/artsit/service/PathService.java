package pl.artsit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.artsit.model.FinalGoalData;
import pl.artsit.model.PathData;
import pl.artsit.model.QuantitativeGoalData;
import pl.artsit.model.database.FinalGoal;
import pl.artsit.model.database.Path;
import pl.artsit.model.database.QuantitativeGoal;
import pl.artsit.repository.database.PathRepository;

import java.util.ArrayList;

@Service
public class PathService {
    @Autowired
    private PathRepository pathRepo;

    @Autowired
    private TargetService targetService;

    public ArrayList<Path> getPaths(){
        return pathRepo.getPaths();
    }

    public Integer setPath(PathData p){
        ArrayList<Path> paths = pathRepo.getPath(p.getId_path());
        Integer flag = 0;
        targetService.deleteFinalGoals(p.getId_user());
        targetService.deleteQuantitativeGoals(p.getId_user());
        for(Path path: paths){
            if(!path.getType()){
                targetService.addFinalGoal(
                        new FinalGoalData(
                            p.getId_user(), true,
                            path.getName(), path.getDescription(),
                            path.getGoal(), path.getDays()
                        )
                );
            }
            else{
                targetService.addQuantitativeGoal(
                  new QuantitativeGoalData(
                        path.getName(), path.getDescription(),
                          true, p.getId_user(),
                        path.getDays(), path.getGoal(),
                        path.getStep()
                  )
                );
            }
        }
        return flag;
    }
}
