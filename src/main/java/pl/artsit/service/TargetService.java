package pl.artsit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.artsit.model.*;
import pl.artsit.model.database.*;
import pl.artsit.repository.database.FinalGoalRepository;
import pl.artsit.repository.database.PredefinedFinalGoalRepository;
import pl.artsit.repository.database.PredefinedQuantitativeGoalRepository;
import pl.artsit.repository.database.QuantitativeGoalRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Service
public class TargetService {
    @Autowired
    private FinalGoalRepository finalGoals;
    @Autowired
    private PredefinedFinalGoalRepository preFinalGoals;
    @Autowired
    private PredefinedQuantitativeGoalRepository predefinedQuantitativeGoal;
    @Autowired
    private QuantitativeGoalRepository quantitativeGoal;
    @Autowired
    private UserService userService;

    public ArrayList<FinalGoalFlag> getFinalGoals(Integer idUser){
        ArrayList<FinalGoalFlag> goals = new ArrayList<>();
        for(FinalGoal goal: finalGoals.getFinalGoals(idUser)){
            String dateStr = (goal.getDate()).toString();
            Integer flag = getDate().compareDates(
                    new ServerDate(
                            Integer.parseInt(dateStr.substring(0,4)),
                            Integer.parseInt(dateStr.substring(5,7)),
                            Integer.parseInt(dateStr.substring(8,10))
                    )
            );
            System.out.println(goal.getName() + "    " + flag);
            if(flag >= goal.getDays() + 1 || flag < 0){
                flag = -1;
            }
            else{
                if(flag > -1){
                    // dodanie flagi -2, gdy odczytane
                    ArrayList<Integer> nums = strToArray(goal.getProgress());
                    if(flag < nums.size() && nums.get(flag) != 0) {
                        flag = -2;
                    }
                }
            }
            goals.add(new FinalGoalFlag(goal, flag));
        }
        return goals;
    }

    public ArrayList<PredefinedFinalGoal> getPredefinedFinalGoals(){
        return preFinalGoals.getPredefinedFinalGoals();
    }

    private String makeReasonString(Integer n){
        String result = "";
        if(n - 1 >= 0){
            int i;
            for(i = 0; i < n - 1; i++ ){
                result += "0,";
            }
            result += "0";
        }
        return result;
    }

    public FinalGoal addFinalGoal(FinalGoalData finalGoal){
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        String input = Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.MONTH) + 1 ) + "-" + (Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        Date t;
        FinalGoal temp = null;

        try {
            t = ft.parse(input);
            finalGoals.addFinalGoal(
                    finalGoal.getName(), finalGoal.getDescription(), finalGoal.getGoal(),
                    finalGoal.getDays(),finalGoal.getDays() * 10, finalGoal.getIs_shared(),
                    makeReasonString(finalGoal.getDays()), finalGoal.getId_user(), t
            );

           temp = new FinalGoal(
                    0, finalGoal.getName() ,finalGoal.getDescription(), finalGoal.getGoal(),
                    finalGoal.getDays(),finalGoal.getDays() * 10, finalGoal.getIs_shared(),
                    makeReasonString(finalGoal.getDays()), finalGoal.getId_user(), t
            );
        } catch (ParseException e) {
            System.out.println("Unparseable using " + ft);
        }

        return temp;
    }

    public ArrayList<FinalGoal> getSharedFinalGoals(Integer id){
        return finalGoals.getSharedFinalGoals(id);
    }

    public void delteFinalGoal(Integer id){
        finalGoals.deleteFinalGoal(id);
    }

    public ArrayList<PredefinedQuantitativeGoal> getPredefinedQuantitativeGoals(){
        return predefinedQuantitativeGoal.getPredefinedQuantitativeGoals();
    }

    public ArrayList<QuantitativeGoalFlag> getQuantitativeGoals(Integer id){
        ArrayList<QuantitativeGoalFlag> goals = new ArrayList<>();
        for(QuantitativeGoal goal: quantitativeGoal.getQuantitativeGoals(id)){
            String dateStr = (goal.getDate()).toString();
            Integer flag = getDate().compareDates(
                    new ServerDate(
                            Integer.parseInt(dateStr.substring(0,4)),
                            Integer.parseInt(dateStr.substring(5,7)),
                            Integer.parseInt(dateStr.substring(8,10))
                    )
            );
            if(flag >= goal.getDays() + 1|| flag < 0){
                flag = -1;
            }
            else{
                if(flag > -1){
                    // dodanie flagi -2, gdy odczytane
                    ArrayList<Integer> nums = strToArray(goal.getProgress());
                    if(flag < nums.size() && nums.get(flag) != 0) {
                        flag = -2;
                    }
                }
            }
            goals.add(new QuantitativeGoalFlag(goal, flag));
        }
        return goals;
    }

    private ArrayList<Integer>  strToArray(String str){
        ArrayList<Integer> nums = new ArrayList<>();
        String temp = "";
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) != ','){
                temp += str.charAt(i);
            }
            else{
                nums.add(Integer.parseInt(temp));
                temp = "";
            }
        }
        return nums;
    }

    public ArrayList<QuantitativeGoal> getSharedQuantitativeGaols(Integer id){
        return quantitativeGoal.getSharedQuantitativeGoals(id);
    }

    public void delteQuantitativeGoal(Integer id){
        quantitativeGoal.deleteQuantitativeGoal(id);
    }

    public QuantitativeGoal addQuantitativeGoal(QuantitativeGoalData goal){
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        String input = Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "-" + (Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        Date dateN;
        QuantitativeGoal temp = null;
        try {
            dateN = ft.parse(input);
            quantitativeGoal.addQuantitativeGoal(
                    goal.getName(), goal.getDescription(),  goal.getGoal(),
                    goal.getDays(), goal.getDays() * 10, goal.getIs_shared(),
                    this.makeReasonString(goal.getDays()), goal.getId_user(), dateN,
                    goal.getStep(), goal.getDays() * goal.getStep()
            );
            temp = new QuantitativeGoal(
                    0, goal.getName(), goal.getDescription(),
                    goal.getDays() * 10, goal.getIs_shared(),
                    goal.getId_user(), goal.getDays(), goal.getGoal(), makeReasonString(goal.getDays()),
                    goal.getDays() * goal.getStep(), goal.getStep(), dateN
            );
        } catch (ParseException e) {
            System.out.println("Unparseable using " + ft);
        }

        return temp;
    }

    public FinalGoal getFinalGoal(Integer id){
        return finalGoals.getFinalGoalWithId(id);
    }

    public Integer updateFGoal(Integer id){
        FinalGoal goalOld = getFinalGoal(id);
        String dateStr = goalOld.getDate().toString();

        // Sprawdzanie warunku
        Integer flag = getDate().compareDates(
                new ServerDate(
                        Integer.parseInt(dateStr.substring(0,4)),
                        Integer.parseInt(dateStr.substring(5,7)),
                        Integer.parseInt(dateStr.substring(8,10))
                )
        );
        if(flag >= goalOld.getDays() + 1 || flag < 0){
            flag = -1;
        }
        if(flag > -1){
            String str = goalOld.getProgress();
            // System.out.println("WEJŚCIOWY PROGRESS  " + str);
            ArrayList<Integer> nums = new ArrayList<>();
            Integer x = 1;
            String temp = "";
            for (int i = 0; i < str.length(); i++) {

                if(str.charAt(i) != ','){
                    temp += str.charAt(i);
                }
                else{
                    nums.add(Integer.parseInt(temp));
                    temp = "";
                }
            }
            nums.add(Integer.parseInt(temp));
            if(nums.get(flag) == 0) {
                nums.set(flag, x);
                // Łączenie tablicy w string wynikowy
                String result = "";
                for (int i = 0; i < nums.size(); i++) {
                    if(i < nums.size() - 1){
                        result += nums.get(i) + ",";
                    }
                    else{
                        result += nums.get(i);
                    }
                }
                // System.out.println("WYJŚCIOWY PROGRESS  " + result);
                userService.addPoints(goalOld.getId_user(), 10);
                return finalGoals.updateProgress(id, result);
            }
            else{
                return -2; // NIE WYMAGA AKTUALIZACJI, WARTOSC PRZYPISANA
            }
        }
        return null;
    }

    public ServerDate getDate(){
        return new ServerDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH) + 1, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
    }

    public Integer updateQGoal(QuantitativeGoalProgress goal){
        QuantitativeGoal goalOld = getQuantitativeGoal(goal.getId());
        String dateStr = goalOld.getDate().toString();
        // Sprawdzanie warunku
        Integer flag = getDate().compareDates(
                new ServerDate(
                        Integer.parseInt(dateStr.substring(0,4)),
                        Integer.parseInt(dateStr.substring(5,7)),
                        Integer.parseInt(dateStr.substring(8,10))
                )
        );
        if(flag >= goalOld.getDays() + 1 || flag < 0){
            flag = -1;
        }
        if(flag > -1){
            String str = goalOld.getProgress();
            System.out.println("WEJŚCIOWY PROGRESS  " + str);
            ArrayList<Integer> nums = new ArrayList<>();
            String temp = "";
            for (int i = 0; i < str.length(); i++) {
                if(str.charAt(i) != ','){
                    temp += str.charAt(i);
                }
                else{
                    nums.add(Integer.parseInt(temp));
                    temp = "";
                }
            }
            nums.add(Integer.parseInt(temp));
            if(nums.get(flag) == 0) {
                nums.set(flag, goal.getVal());
                String result = "";
                for (int i = 0; i < nums.size(); i++) {
                    if(i < nums.size() - 1){
                        result += nums.get(i) + ",";
                    }
                    else{
                        result += nums.get(i);
                    }
                }
                Integer p = 0;
                if(goal.getVal() >= goalOld.getStep()){
                    p = 10;
                }
                else {
                    p = ((goal.getVal() * 100 / goalOld.getStep()) * 10) / 100;
                }
                userService.addPoints(goalOld.getId_user(), p);
                System.out.println("WYJŚCIOWY PROGRESS  " + result);
                return quantitativeGoal.updateQProgress(goal.getId(), result);
            }
            else{
                return -2;
            }
        }
        return null;
    }

    public QuantitativeGoal getQuantitativeGoal(Integer id){
        return quantitativeGoal.getQuantitativeGoalWithId(id);
    }

    // TEMPORARY
    public Integer updateFGoalTemp(Integer id, String p){
        return finalGoals.updateProgress(id,p);
    }

    public Integer updateQGoalTemp(Integer id, String p){
        return quantitativeGoal.updateQProgress(id,p);
    }

    public Integer updateFGoalData(FinalGoalUpdateData goal){
        return finalGoals.updateFGoalData(
                goal.getId(), goal.getName(), goal.getDescription(),
                goal.getIs_shared(), goal.getGoal(), goal.getDays()
        );
    }

    public Integer updateQGoalData(QuantitativeGoalUpdateData goal){
        return quantitativeGoal.updateQGoalData(
                goal.getId(), goal.getName(), goal.getDescription(),
                goal.getIs_shared(), goal.getGoal(), goal.getDays(),
                goal.getStep()
        );
    }

    // DLa sciezek
    public Integer deleteFinalGoals(Integer id){
        return finalGoals.deleteFinalGoals(id);
    }

    public Integer deleteQuantitativeGoals(Integer id){
        return quantitativeGoal.deleteQuantitativeGoals(id);
    }
}
