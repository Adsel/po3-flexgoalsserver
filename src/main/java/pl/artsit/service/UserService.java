package pl.artsit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.artsit.model.ServerDate;
import pl.artsit.model.database.FinalGoal;
import pl.artsit.model.database.User;
import pl.artsit.model.database.UserData;
import pl.artsit.repository.database.UserDataRepository;
import pl.artsit.repository.database.UserRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDataRepository userDataRepository;

    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    public User login(String login, String password){
        User found = userRepository.login(login);
        if(found != null){
            if(encoder.matches(password, found.getPassword())){
                return found;
            }
        }
        return null;
    }

    public User register(User newUser) throws ParseException {
       userRepository.insertUser(newUser.getLogin(), encoder.encode(newUser.getPassword()), newUser.getEmail(), 0);
       User registered = userRepository.login(newUser.getLogin());
       userRepository.insertUserData(registered.getId(), null, new SimpleDateFormat("yyyy-MM-dd").parse("1800-01-01"), false);
       return registered;
    }

    public Boolean checkEmail(String email) {
        String checked = userRepository.checkEmail(email);
        if (checked != null && checked != ""){
            return false;
        }
        return true;
    }

    public UserData getUserPrivateData(Integer id){
        return userDataRepository.getUserPrivateData(id);
    }

    public UserData updateUserName(UserData data){
        userDataRepository.updateUserName(data.getId(), data.getName());
        return data;
    }

    public UserData updateUserSex(UserData data){
        userDataRepository.updateUserSex(data.getId(), data.getSex());
        return data;
    }

    public UserData updateUserDate(UserData data){
        userDataRepository.updateUserDate(data.getId(), data.getDate_of_birth());
        return data;
    }

    public ServerDate getDate(){
        return new ServerDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
    }

    public Integer getPoints(Integer id){
        return userRepository.getPoints(id);
    }

    public Integer addPoints(Integer id, Integer val){
        return userRepository.updatePoints(id,userRepository.getPoints(id) + val);
    }
}
