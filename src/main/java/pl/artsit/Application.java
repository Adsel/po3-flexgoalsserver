package pl.artsit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.artsit.repository.database.UserRepository;
import pl.artsit.service.TargetService;

@SpringBootApplication
public class Application implements CommandLineRunner {
    @Autowired
    private TargetService t;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println("Start Application...");
    }
}
