package pl.artsit.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.artsit.model.*;
import pl.artsit.model.database.*;
import pl.artsit.service.PathService;
import pl.artsit.service.TargetService;
import pl.artsit.service.UserService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DatabaseController {
    @Autowired
    private UserService userService;

    @Autowired
    private TargetService targetService;

    @Autowired
    private PathService pathService;

    @CrossOrigin
    @PostMapping(value = "/users/login-user")
    public ResponseEntity<?> loginUser(@RequestBody AuthData authData) {
        System.out.println("---Try to login guest with this data: " + authData.getLogin() + " " + authData.getPassword());
        User logged = userService.login(authData.getLogin(), authData.getPassword());

        return logged != null ?
                new ResponseEntity<>(logged, HttpStatus.OK) :
                new ResponseEntity<>("Dane logowania niepoprawne", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/users/points/{id}")
    public ResponseEntity<?> getPoints(@PathVariable Integer id) {
        Integer flag = userService.getPoints(id);
        System.out.println("---Try to get points! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania liczby punktów!", HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/users/register-user")
    public ResponseEntity<?> loginUser(@RequestBody User user) {

        User registered = null;
        Boolean flagmail = true;
        if(userService.checkEmail(user.getEmail())){
            try {
                registered = userService.register(user);
                flagmail = false;
            }
            catch (ParseException pe){
                //
            }
        }
        System.out.println("---Try to register guest");

        if(flagmail){
            return  new ResponseEntity<>("Email zajęty!", HttpStatus.OK);
        }
        return registered != null ?
                new ResponseEntity<>(registered, HttpStatus.OK) :
                new ResponseEntity<>("Rejestracja nieudana!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/users/priv-data/{id}")
    public ResponseEntity<?> getUserPrivateData(@PathVariable Integer id) {
        UserData data = userService.getUserPrivateData(id);
        System.out.println("---Try to get user data! " + id);

        return data != null ?
                new ResponseEntity<>(data, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania danych użytkownika!", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/users/priv-data/update-name")
    public ResponseEntity<?> updateUserName(@RequestBody UserData data) {
        UserData flag = userService.updateUserName(data);
        System.out.println("---Try to update user name! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji imienia!", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/users/priv-data/update-sex")
    public ResponseEntity<?> updateUserSex(@RequestBody UserData data) {
        UserData flag = userService.updateUserSex(data);
        System.out.println("---Try to update user sex! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji płci!", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/users/priv-data/update-date")
    public ResponseEntity<?> updateUserDate(@RequestBody UserData data) {
        UserData flag = userService.updateUserDate(data);
        System.out.println("---Try to update user date of birth! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji daty urodzenia!", HttpStatus.OK);
    }

    // GOALS
    @CrossOrigin
    @GetMapping(value = "/goals/predefined-final")
    public ResponseEntity<?> getPredefinedFinal() {
        ArrayList<PredefinedFinalGoal> finalGoals = targetService.getPredefinedFinalGoals();
        System.out.println("---Try to get all predefined goals! ");

        return finalGoals != null ?
                new ResponseEntity<>(finalGoals, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania predefiniowanych celów!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/goals/final/{id}")
    public ResponseEntity<?> getFinal(@PathVariable Integer id) {
        List<FinalGoalFlag> finalGoals = targetService.getFinalGoals(id);
        System.out.println("---Try to get all active goals! ");

        return finalGoals != null ?
                new ResponseEntity<>(finalGoals, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania aktywnych celów zaliczeniowych!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/goals/final-id/{id}")
    public ResponseEntity<?> getFinalWithId(@PathVariable Integer id) {
        FinalGoal goal = targetService.getFinalGoal(id);
        System.out.println("---Try to get all active goals! ");

        return goal != null ?
                new ResponseEntity<>(goal, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania konkretnego celu!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/goals/final-shared/{id}")
    public ResponseEntity<?> getFinalShared(@PathVariable Integer id) {
        List<FinalGoal> finalGoals = targetService.getSharedFinalGoals(id);
        System.out.println("---Try to get shared active goals! ");

        return finalGoals != null ?
                new ResponseEntity<>(finalGoals, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania aktywnych celów zaliczeniowych!", HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/goals/final-add")
    public ResponseEntity<?> addFinalGoal(@RequestBody FinalGoalData finalGoal) {
        FinalGoal addedFinalGoal = targetService.addFinalGoal(finalGoal);
        System.out.println("---Try to add final goal");

        return addedFinalGoal != null ?
                new ResponseEntity<>(addedFinalGoal, HttpStatus.OK) :
                new ResponseEntity<>("Dane logowania niepoprawne", HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping(value = "/goals/final-delete/{id}")
    public ResponseEntity<?> delFinal(@PathVariable Integer id) {
        targetService.delteFinalGoal(id);
        System.out.println("---Try to delete active goal with id: " + id);

        return new ResponseEntity<>(null, HttpStatus.OK);

    }

    @CrossOrigin
    @PutMapping(value = "/goals/update-prog-fgoal")
    public ResponseEntity<?> updateGoalProgress(@RequestBody Integer goal) {
        System.out.println("---Try to update goal progress!");
        Integer flag = targetService.updateFGoal(goal);


        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji postępu!", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/goals/update-fgoal")
    public ResponseEntity<?> updateFGoalData(@RequestBody FinalGoalUpdateData goal) {
        Integer flag = targetService.updateFGoalData(goal);
        System.out.println("---Try to update final goal data! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji danych celu zaliczeniowego!", HttpStatus.OK);
    }

    // QUANTITATIVE GOALS
    @CrossOrigin
    @GetMapping(value = "/goals/predefined-quantitative")
    public ResponseEntity<?> getPredefinedQuantitative() {
        List<PredefinedQuantitativeGoal> goals = targetService.getPredefinedQuantitativeGoals();
        System.out.println("---Try to get all predefined quantitative goals! ");

        return goals != null ?
                new ResponseEntity<>(goals, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania predefiniowanych celów!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/goals/quantitative/{id}")
    public ResponseEntity<?> getQuantitative(@PathVariable Integer id) {
        ArrayList<QuantitativeGoalFlag> goals = targetService.getQuantitativeGoals(id);
        System.out.println("---Try to get all active quantitative goals! ");

        return goals != null ?
                new ResponseEntity<>(goals, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania aktywnych celów ilościowych!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/goals/quantitative-id/{id}")
    public ResponseEntity<?> getQuantitativeWithId(@PathVariable Integer id) {
        QuantitativeGoal goal = targetService.getQuantitativeGoal(id);
        System.out.println("---Try to get all quantitative goal with id! ");

        return goal != null ?
                new ResponseEntity<>(goal, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania konkretnego ilościowego celu!", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/goals/quantitative-shared/{id}")
    public ResponseEntity<?> getQuantitativeShared(@PathVariable Integer id) {
        ArrayList<QuantitativeGoal> goals = targetService.getSharedQuantitativeGaols(id);
        System.out.println("---Try to get shared active quantitative goals! ");

        return goals != null ?
                new ResponseEntity<>(goals, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania aktywnych celów ilościowych!", HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping(value = "/goals/quantitative-delete/{id}")
    public ResponseEntity<?> delQuantitative(@PathVariable Integer id) {
        targetService.delteQuantitativeGoal(id);
        System.out.println("---Try to delete active quantitative goal with id: " + id);

        return new ResponseEntity<>(null, HttpStatus.OK);

    }

    @CrossOrigin
    @PostMapping(value = "/goals/quantitative-add")
    public ResponseEntity<?> addQuantitativeGoal(@RequestBody QuantitativeGoalData quantitativeGoal) {
        System.out.println("---Try to add quantitative goal ");
        QuantitativeGoal addedQuantitativeGoal = targetService.addQuantitativeGoal(quantitativeGoal);


        return addedQuantitativeGoal != null ?
                new ResponseEntity<>(addedQuantitativeGoal, HttpStatus.OK) :
                new ResponseEntity<>("Dane logowania niepoprawne", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping(value = "/goals/update-qgoal")
    public ResponseEntity<?> updateQGoalProgress(@RequestBody QuantitativeGoalProgress goal) {
        Integer flag = targetService.updateQGoal(goal);
        System.out.println("---Try to update quantitative goal progress! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji postępu dla celu ilościowego!", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/goals/update-qgoaldata")
    public ResponseEntity<?> updateQGoalData(@RequestBody QuantitativeGoalUpdateData goal) {
        Integer flag = targetService.updateQGoalData(goal);
        System.out.println("---Try to update quantitative goal data! ");

        return flag != null ?
                new ResponseEntity<>(flag, HttpStatus.OK) :
                new ResponseEntity<>("Błąd aktualizacji danych celu ilościowego!", HttpStatus.OK);
    }

    // PATHS
    @CrossOrigin
    @GetMapping(value = "/paths/predefined")
    public ResponseEntity<?> getPaths() {
        ArrayList<Path> paths = pathService.getPaths();
        System.out.println("---Try to get all predefined paths! ");

        return paths != null ?
                new ResponseEntity<>(paths, HttpStatus.OK) :
                new ResponseEntity<>("Błąd pobrania predefiniowanych ścieżek!", HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/paths/set")
    public ResponseEntity<?> setPath(@RequestBody PathData path) {
        Integer paths = pathService.setPath(path);
        System.out.println("---Try to set path! ");

        return paths != null ?
                new ResponseEntity<>(paths, HttpStatus.OK) :
                new ResponseEntity<>("Błąd ustawienia ścieżki", HttpStatus.OK);
    }

}
